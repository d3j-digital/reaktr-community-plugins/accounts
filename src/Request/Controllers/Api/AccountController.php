<?php

namespace D3JDigital\Accounts\Request\Controllers\Api;

use D3JDigital\Accounts\Response\Resources\AccountResource;
use Illuminate\Http\JsonResponse;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Controller;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\RoutingService;
use D3JDigital\Accounts\Contracts\Services\AccountServiceInterface;
use D3JDigital\Accounts\Request\Validation\StoreAccount;
use D3JDigital\Accounts\Request\Validation\UpdateAccount;

class AccountController extends Controller
{
    /**
     * @param RoutingService $routingService
     * @param AccountServiceInterface $accountService
     */
    public function __construct(
        protected RoutingService $routingService,
        protected AccountServiceInterface $accountService,
    ) {
        parent::__construct();
        $this->middleware('UserIsAuthenticated:jwt');
        $this->middleware('UserIsAuthorised:accounts.index')->only('index');
        $this->middleware('UserIsAuthorised:accounts.show')->only('show');
        $this->middleware('UserIsAuthorised:accounts.store')->only('store');
        $this->middleware('UserIsAuthorised:accounts.update')->only('update');
        $this->middleware('UserIsAuthorised:accounts.destroy')->only('destroy');
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return $this->routingService->renderJsonSuccessResponse(200, '', AccountResource::collection($this->accountService->getAccounts($this->relations, $this->columns)));
    }

    /**
     * @param StoreAccount $request
     * @return JsonResponse
     */
    public function store(StoreAccount $request): JsonResponse
    {
        return $this->routingService->renderJsonSuccessResponse(201, '', new AccountResource($this->accountService->createAccount($request->validated())));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        if ($resource = $this->accountService->getAccount($id, $this->relations, $this->columns)) {
            return $this->routingService->renderJsonSuccessResponse(200, '', new AccountResource($resource));
        }
        return $this->routingService->renderJsonErrorResponse(404);
    }

    /**
     * @param UpdateAccount $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(UpdateAccount $request, int $id): JsonResponse
    {
        if ($resource = $this->accountService->updateAccount($id, $request->validated())) {
            return $this->routingService->renderJsonSuccessResponse(202, '', new AccountResource($resource));
        }
        return $this->routingService->renderJsonErrorResponse(204);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $this->accountService->deleteAccount($id);
        return $this->routingService->renderJsonSuccessResponse(204);
    }
}
