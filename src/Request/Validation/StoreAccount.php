<?php

namespace D3JDigital\Accounts\Request\Validation;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use D3JDigital\Accounts\Response\Entities\AccountEntity;

class StoreAccount extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'source' => ['required', Rule::in(AccountEntity::getAvailableSources())],
            'type' => ['required', Rule::in(AccountEntity::getAvailableTypes())],
            'sub_type' => ['required', Rule::in(AccountEntity::getAvailableSubTypes())],
            'name' => 'required|string',
            'company_reg_number' => 'sometimes|string',
            'company_vat_number' => 'sometimes|string',
            'primary_contact_first_name' => 'sometimes|string',
            'primary_contact_last_name' => 'sometimes|string',
            'primary_contact_phone_country_code' => 'sometimes|required_with:primary_contact_phone_number|integer',
            'primary_contact_phone_number' => 'sometimes|string',
            'status' => ['required', Rule::in(AccountEntity::getAvailableStatuses())],
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'required' => 'this field is required',
            'source.in' => 'you can only specify one of the following accepted sources (' . implode(',', AccountEntity::getAvailableSources()) . ')',
            'type.in' => 'you can only specify one of the following accepted types (' . implode(',', AccountEntity::getAvailableTypes()) . ')',
            'sub_type.in' => 'you can only specify one of the following accepted sub types (' . implode(',', AccountEntity::getAvailableSubTypes()) . ')',
            'status.in' => 'you can only specify one of the following accepted statuses (' . implode(',', AccountEntity::getAvailableStatuses()) . ')',
        ];
    }
}
