<?php

namespace D3JDigital\Accounts\Services;

use Illuminate\Support\Collection;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Entity;
use OpenSourceDeveloper\Reaktr\Core\Response\LengthAwarePaginator;
use OpenSourceDeveloper\Reaktr\Core\Response\PaginationResponse;
use D3JDigital\Accounts\Contracts\Services\AccountServiceInterface;
use D3JDigital\Accounts\Database\Eloquent\Repositories\AccountRepository;
use D3JDigital\Accounts\Response\Entities\AccountEntity;

class AccountService implements AccountServiceInterface
{
    public function __construct(
        protected AccountRepository $accountRepository,
    ) {}

    public function getAccounts(array $relations = [], array $columns = ['*'], bool $paginate = true): LengthAwarePaginator|Collection
    {
        $result = $this->accountRepository->all($relations, $columns, $paginate);
        if ($result instanceof PaginationResponse) {
            return new LengthAwarePaginator(collect($result->getRecords())->mapInto(AccountEntity::class), $result->getRecordCount(), $result->getRecordsPerPage(), $result->getCurrentPage());
        }
        return collect($result)->mapInto(AccountEntity::class);
    }

    public function getAccount(int $id, array $relations = [], array $columns = ['*']): ?Entity
    {
        $result = $this->accountRepository->find($id, $relations, $columns);
        return $result ? (new AccountEntity)->fill($result) : null;
    }

    public function createAccount(array $attributes): ?Entity
    {
        $result = $this->accountRepository->create(array_merge($attributes, ['reference' => 'acc_' . app()->generateUniqueReference()]));
        return $result ? (new AccountEntity)->fill($result) : null;
    }

    public function updateAccount(int $id, array $attributes, bool $withoutLoadingModel = false): ?Entity
    {
        $result = $this->accountRepository->update($id, \Arr::except($attributes, 'reference'), $withoutLoadingModel);
        return $result ? (new AccountEntity)->fill($result) : null;
    }

    public function deleteAccount(int|array $ids, bool $forceDelete = false): void
    {
        $this->accountRepository->delete($ids, $forceDelete);
    }
}
