<?php

namespace D3JDigital\Accounts\Contracts\Repositories;

use OpenSourceDeveloper\Reaktr\Core\Contracts\Repositories\Repository;

interface AccountRepositoryInterface extends Repository
{

}
