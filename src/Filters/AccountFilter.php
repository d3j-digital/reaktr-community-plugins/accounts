<?php

namespace D3JDigital\Accounts\Filters;

class AccountFilter
{
    const GET_ACCOUNT_SOURCES = 'plugin.accounts.filter.get-account-sources';
    const GET_ACCOUNT_TYPES = 'plugin.accounts.filter.get-account-types';
    const GET_ACCOUNT_SUB_TYPES = 'plugin.accounts.filter.get-account-sub-types';
    const GET_ACCOUNT_STATUSES = 'plugin.accounts.filter.get-account-statuses';
    const PREPARE_ACCOUNT_RESOURCE = 'plugin.accounts.filter.prepare-account-resource';
}
