<?php

namespace D3JDigital\Accounts\Enums;

enum AccountSubType
{
    case RETAIL;
    case TRADE;
}
