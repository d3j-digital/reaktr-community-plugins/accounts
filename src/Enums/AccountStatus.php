<?php

namespace D3JDigital\Accounts\Enums;

enum AccountStatus
{
    case ACTIVE;
    case INACTIVE;
    case TEMPORARILY_SUSPENDED;
    case PERMANENTLY_SUSPENDED;
    case PENDING_APPROVAL;
}
