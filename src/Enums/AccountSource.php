<?php

namespace D3JDigital\Accounts\Enums;

enum AccountSource
{
    case MANUAL;
    case IMPORT;
    case API;
}
