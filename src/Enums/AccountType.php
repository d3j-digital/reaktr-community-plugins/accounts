<?php

namespace D3JDigital\Accounts\Enums;

enum AccountType
{
    case CUSTOMER;
    case SUPPLIER;
}
