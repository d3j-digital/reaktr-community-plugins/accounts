<?php

namespace D3JDigital\Accounts\Response\Entities;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Entity;
use D3JDigital\Accounts\Enums\AccountSource;
use D3JDigital\Accounts\Enums\AccountStatus;
use D3JDigital\Accounts\Enums\AccountSubType;
use D3JDigital\Accounts\Enums\AccountType;
use D3JDigital\Accounts\Filters\AccountFilter;

class AccountEntity extends Entity
{
    public ?int $id = null;
    public ?string $reference = null;
    public ?string $source = null;
    public ?string $type = null;
    public ?string $subType = null;
    public ?string $name = null;
    public ?string $companyRegNumber = null;
    public ?string $companyVatNumber = null;
    public ?string $primaryContactFirstName = null;
    public ?string $primaryContactLastName = null;
    public ?string $primaryContactPhoneCountryCode = null;
    public ?string $primaryContactPhoneNumber = null;
    public ?string $primaryContactEmail = null;
    public ?string $status = null;

    /**
     * @return array
     */
    public static function getAvailableSources(): array
    {
        return app()->triggerFilter(AccountFilter::GET_ACCOUNT_SOURCES, collect(AccountSource::cases())->pluck('name')->toArray());
    }

    /**
     * @return array
     */
    public static function getAvailableTypes(): array
    {
        return app()->triggerFilter(AccountFilter::GET_ACCOUNT_TYPES, collect(AccountType::cases())->pluck('name')->toArray());
    }

    /**
     * @return array
     */
    public static function getAvailableSubTypes(): array
    {
        return app()->triggerFilter(AccountFilter::GET_ACCOUNT_SUB_TYPES, collect(AccountSubType::cases())->pluck('name')->toArray());
    }

    /**
     * @return array
     */
    public static function getAvailableStatuses(): array
    {
        return app()->triggerFilter(AccountFilter::GET_ACCOUNT_STATUSES, collect(AccountStatus::cases())->pluck('name')->toArray());
    }
}
