<?php

namespace D3JDigital\Accounts\Response\Resources;

use OpenSourceDeveloper\Reaktr\Core\Response\Resources\JsonResource;
use D3JDigital\Accounts\Filters\AccountFilter;
use D3JDigital\Accounts\Response\Entities\AccountEntity;

/**
 * @mixin AccountEntity
 */
class AccountResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'type' => 'account',
            'attributes' => [
                'id' => $this->id,
                'reference' => $this->reference,
                'source' => $this->source,
                'type' => $this->type,
                'sub_type' => $this->subType,
                'name' => $this->name,
                'company_reg_number' => $this->companyRegNumber,
                'company_vat_number' => $this->companyVatNumber,
                'primary_contact_first_name' => $this->primaryContactFirstName,
                'primary_contact_last_name' => $this->primaryContactLastName,
                'primary_contact_phone_country_code' => $this->primaryContactPhoneCountryCode,
                'primary_contact_phone_number' => $this->primaryContactPhoneNumber,
                'primary_contact_email' => $this->primaryContactEmail,
                'status' => $this->status,
                'created_at' => $this->createdAt,
                'updated_at' => $this->updatedAt,
                'deleted_at' => $this->deletedAt,
            ],
        ];

        return app()->triggerFilter(AccountFilter::PREPARE_ACCOUNT_RESOURCE, $data, $this);
    }
}
