<?php

namespace D3JDigital\Accounts;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Plugin as ReaktrPlugin;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\RoutingService;
use D3JDigital\Accounts\Contracts\Repositories\AccountRepositoryInterface;
use D3JDigital\Accounts\Contracts\Services\AccountServiceInterface;
use D3JDigital\Accounts\Database\Eloquent\Repositories\AccountRepository;
use D3JDigital\Accounts\Services\AccountService;

class Plugin extends ReaktrPlugin
{
    public ?string $pluginName = 'Accounts';
    public ?string $pluginKey = 'accounts';
    public ?string $pluginVendorName = 'D3JDigital';
    public ?string $pluginVendorKey = 'd3j-digital';
    public ?string $pluginVersion = 'v0.0.1';
    public ?string $pluginDescription = 'Adds account functionality';
    public ?array $pluginDependencies = [];

    public array $singletons = [
        AccountRepositoryInterface::class => AccountRepository::class,
        AccountServiceInterface::class => AccountService::class,
    ];

    public function registerRoutes(): void
    {
        $routingService = app(RoutingService::class);
        $routingService->registerApiRoutes('D3JDigital\Accounts\Request\Controllers\Api', __DIR__ . '/Request/Routes/Api/v1.php', 1);
    }

    public function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/Database/Migrations');
    }
}
