<?php

namespace D3JDigital\Accounts\Database\Eloquent\Repositories;

use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Model;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Repository;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\SystemModel;
use D3JDigital\Accounts\Contracts\Repositories\AccountRepositoryInterface;
use D3JDigital\Accounts\Database\Eloquent\Models\Account;

class AccountRepository extends Repository implements AccountRepositoryInterface
{
    /**
     * @return Model|SystemModel
     */
    function model(): Model|SystemModel
    {
        return new Account();
    }
}
