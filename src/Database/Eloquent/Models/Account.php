<?php

namespace D3JDigital\Accounts\Database\Eloquent\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use OpenSourceDeveloper\Reaktr\Core\Abstracts\Eloquent\Model;

class Account extends Model
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'accounts';

    /**
     * @var array
     */
    protected $guarded = [];
}
