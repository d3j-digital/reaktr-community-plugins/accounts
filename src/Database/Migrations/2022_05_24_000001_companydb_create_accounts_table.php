<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CompanydbCreateAccountsTable extends Migration
{
    public function up(): void
    {
        if (!Schema::hasTable('accounts')) {
            Schema::create('accounts', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('reference');
                $table->string('source');
                $table->string('type');
                $table->string('sub_type');
                $table->string('name');
                $table->string('company_reg_number')->nullable();
                $table->string('company_vat_number')->nullable();
                $table->string('primary_contact_first_name')->nullable();
                $table->string('primary_contact_last_name')->nullable();
                $table->integer('primary_contact_phone_country_code')->nullable();
                $table->string('primary_contact_phone_number')->nullable();
                $table->string('primary_contact_email')->nullable();
                $table->string('status');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    public function down(): void
    {
        Schema::dropIfExists('accounts');
    }
}
