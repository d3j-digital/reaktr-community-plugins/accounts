<?php

use Illuminate\Database\Migrations\Migration;
use OpenSourceDeveloper\Reaktr\Core\Contracts\Services\AuthService;
use OpenSourceDeveloper\Reaktr\Core\Base\PermissionObject;

class CompanydbAddAccountPermissions extends Migration
{
    protected AuthService $authService;

    public function __construct()
    {
        $this->authService = app(AuthService::class);
    }

    public function up(): void
    {
        $permissions = [
            new PermissionObject('accounts.*', 'reaktr.permissions.global.all_actions', ['resource' => 'plugin-accounts.resources.account.name.singular']),
            new PermissionObject('accounts.index', 'reaktr.permissions.global.list', ['resource' => 'plugin-accounts.resources.account.name.singular']),
            new PermissionObject('accounts.store', 'reaktr.permissions.global.create', ['resource' => 'plugin-accounts.resources.account.name.singular']),
            new PermissionObject('accounts.show', 'reaktr.permissions.global.view', ['resource' => 'plugin-accounts.resources.account.name.singular']),
            new PermissionObject('accounts.update', 'reaktr.permissions.global.update', ['resource' => 'plugin-accounts.resources.account.name.singular']),
            new PermissionObject('accounts.destroy', 'reaktr.permissions.global.delete', ['resource' => 'plugin-accounts.resources.account.name.singular']),
        ];

        foreach ($permissions as $permission) {
            $this->authService->createPermission($permission);
        }
    }
}
